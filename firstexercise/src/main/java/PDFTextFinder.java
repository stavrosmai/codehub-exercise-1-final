import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class PDFTextFinder {

    public Map<String, Boolean> findTextInPDFDocument(File file, List<String> inputList) throws Exception {
        Map<String, Boolean> outputMap = new HashMap<String, Boolean>();

        PDFTextStripper stripper = new PDFTextStripper();
        // Extracts the text
        String text = stripper.getText(PDDocument.load(file)); // since this is a static method you can invoke it using class name.

        // Iterate the List to see if exists in the text
        Iterator iterator = inputList.iterator();

        while (iterator.hasNext()) {
            String word = iterator.next().toString();
            Boolean exists = text.contains(word);
            outputMap.put(word, exists);
        }
        return outputMap;
    }
}


