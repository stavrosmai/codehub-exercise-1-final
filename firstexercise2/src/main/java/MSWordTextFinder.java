import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MSWordTextFinder {

    public Map<String, Boolean> findTextInWordDocument(File file, List<String> inputList) throws Exception {
        Map<String, Boolean> outputMap = new HashMap<String, Boolean>();

        // Create a XWPFDocument Object first
        XWPFDocument doc = new XWPFDocument(OPCPackage.open(file));
        XWPFWordExtractor extractor = new XWPFWordExtractor(doc);

        // Extracts the text
        String text = extractor.getText();
        System.out.println(text);

        // Iterate the List to see if exists in the text
        Iterator iterator = inputList.iterator();

        while (iterator.hasNext()) {
            String word = iterator.next().toString();
            Boolean exists = text.contains(word);
            outputMap.put(word, exists);
        }
        return outputMap;
    }

}
