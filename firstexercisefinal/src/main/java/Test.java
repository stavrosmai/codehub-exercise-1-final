import org.apache.commons.io.FilenameUtils;
import java.io.File;
import java.util.*;

public class Test {

    public static void main(String[] args) {

        try {

            // Tested with these two Paths
            //C:\\Users\\stavros.maidonis\\IdeaProjects\\firstexercisefinal\\src\\main\\resources\\windows.docx
            //C:\\Users\\stavros.maidonis\\IdeaProjects\\firstexercisefinal\\src\\main\\resources\\windows.pdf

            // The files are under .\resources
            // resources/windows.pdf

            // Test with this list of words
            // [Getting, Started, new, standard, workstations, 2016, numerous, cat, dog, fish, butterfly, shark, Bear, zebra]

            // List initialization
            List<String> stringList = new ArrayList<String>();
            stringList.add("Getting");
            stringList.add("Started");
            stringList.add("new");
            stringList.add("standard");
            stringList.add("workstations");
            stringList.add("2016");
            stringList.add("numerous");
            stringList.add("frog");
            stringList.add("dog"); //
            stringList.add("fish");
            stringList.add("butterfly");
            stringList.add("shark");
            stringList.add("Bear");
            stringList.add("zebra");

            System.out.println("Enter a file path: ");
            Scanner reader = new Scanner(System.in);
            String inputPath = reader.next();
            System.out.println(inputPath);

            String extension = FilenameUtils.getExtension(inputPath);


            if (extension.equals("pdf")) {
                System.out.println("The file is a .pdf");
                File file = new File(inputPath);
                System.out.println(file);

                PDFTextFinder finder = new PDFTextFinder();
                Map<String, Boolean> resultsMap = finder.findTextInPDFDocument(file, stringList);
                Iterator iterator = resultsMap.entrySet().iterator();

                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();
                    System.out.println(pair.getKey() + " : " + pair.getValue());
                }

            } else if (extension.equals("docx")) {
                System.out.println("The file is a .docx");
                File file = new File(inputPath);
                System.out.println(file);

                MSWordTextFinder finder = new MSWordTextFinder();
                Map<String, Boolean> resultsMap = finder.findTextInWordDocument(file, stringList);
                Iterator iterator = resultsMap.entrySet().iterator();

                System.out.println(stringList);

                while (iterator.hasNext()) {
                    Map.Entry pair = (Map.Entry) iterator.next();
                    System.out.println(pair.getKey() + " : " + pair.getValue());
                }
            }

        } catch (Exception e) {
            System.out.println(e);
        }
    }
}
